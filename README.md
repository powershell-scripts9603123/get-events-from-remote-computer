# Get-events-from-remote-computer

## Description

This script is used to validate that a connector server can pull logs from a Windows reporting assets.. This is acheived by attempting to poll event log from Windows devices using Powershell commands. To learn more about Windows event powershell commands, click the [link](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.diagnostics/get-winevent?view=powershell-7.3)


## How to use

- Create a **ServerList.txt** file and populate with the IP addresses of reporting Windows assets, line-by-line.
- Create a new folder, "Credentials". Then ,create a file, Username.txt - enter and save Login username in the file.    
*NB: Do not create a password.txt file. It will be created automatically.*
- Update all file path in the script
- Run **event_viewer.ps1** in Powershell

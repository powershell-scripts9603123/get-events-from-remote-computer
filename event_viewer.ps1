# Purpose: To verify that a connector server is able to poll logs from Windows devices
# Maintainer: William Idakwo
# Date: July 9, 2023

#Validate IP addresses
function Validate-IP ([Parameter(position=0)]$Server) {
    [ipaddress]"$Server"
    if($?){
        return $True
    } else {
        return $False
    }
}

$PasswordPath = 'C:\Users\Jason\Documents\Event_Viewer_Validator\Credentials\password.txt'
$Username = Get-Content "C:\Users\Jason\Documents\Event_Viewer_Validator\Credentials\username.txt"
$ServerList = Get-Content "C:\Users\Jason\Documents\Event_Viewer_Validator\ServerList.txt"
$resultPath = "C:\Users\Jason\Documents\Event_Viewer_Validator\Result.txt"

# Checks if password.txt file exist. If not, prompt for password and store the password securely in a password.txt file. 
if (-not(Test-Path -Path $PasswordPath -PathType Leaf)) {
    (Get-Credential).Password | ConvertFrom-SecureString | Out-File $PasswordPath
} 
$Password = Get-Content $PasswordPath | ConvertTo-SecureString
$Cred = New-Object System.Management.Automation.PsCredential ($Username,$Password)

# An array that holds row item for each iteration below
$results = @()

ForEach ($Server in $ServerList) {

    if(Validate-IP $Server) {
        # Create a hashtable
        $ipResult = @{
            "IPAddress" = $Server
        }
        # Attempts to poll Application logs from Windows devices.
        # Options: To poll system logs use "-ListLog System". To poll both application and system logs use "-ListLog Application,    #System"
        Get-WinEvent -ListLog Application -ComputerName $Server -Credential $Cred

        if ($?) {
            # Command succeeded
            $ipResult.Add("EventViewer", "Successful")
        }
        else {
            # Command failed
            $errMessage = $Error[0].ToString() # $Error[0] holds the error message
            $ipResult.Add("EventViewer", "Failed, $errMessage")
        }
    } else {
        $ipResult.Add("EventViewer", "N/A")
    }

    $results += New-Object psobject -Property $ipResult
}
$results | Format-Table -AutoSize | Out-File $resultPath 
